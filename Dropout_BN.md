### Dropout 缓解过拟合

dropout-随机忽略一些神经元

训练时，使用dropout---model.train()

测试不用，model.eval()



### Batch Normalization 批标准化

避免全连接层计算的结果都在激活函数的饱和区间，效果会很差

使用批标准化，将输入标准化到有效的激活函数区间

可解决梯度消失和梯度爆炸


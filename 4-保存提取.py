import torch

torch.manual_seed(1)  # 固定生成的随机数

# 假数据
x = torch.unsqueeze(torch.linspace(-1, 1, 100), dim=1)
y = x.pow(2) + 0.2 * torch.rand(x.size())


def save():
    net = torch.nn.Sequential(
        torch.nn.Linear(1, 10),
        torch.nn.ReLU(),
        torch.nn.Linear(10, 1)
    )

    optimizer = torch.optim.SGD(net.parameters(), lr=0.5)
    loss_func = torch.nn.MSELoss()

    # 训练
    for t in range(100):
        pred = net(x)
        loss = loss_func(pred, y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print(pred)
    torch.save(net, 'net.pkl')  # 保存网络
    torch.save(net.state_dict(), 'net_params.pkl')  # 保存网络参数


def restore_net():
    net1 = torch.load('net.pkl')
    pred = net1(x)
    print(pred)


def restore_params():
    net2 = torch.nn.Sequential(
        torch.nn.Linear(1, 10),
        torch.nn.ReLU(),
        torch.nn.Linear(10, 1)
    )

    # 在提取参数之前要建一个一模一样的网络
    net2.load_state_dict(torch.load('net_params.pkl'))
    pred = net2(x)
    print(pred)


# save()

# restore_net()   # 提取整个网络
restore_params()  # 仅提取网络参数

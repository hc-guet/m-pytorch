import torch
import numpy as np

np_data = np.arange(6).reshape((2, 3))
torch_data = torch.from_numpy(np_data)
tensor2array = torch_data.numpy()

print(np_data, torch_data, tensor2array)

data = [-1, -2, 1, 2]
tensor = torch.FloatTensor(data)

# abs 绝对值
print(np.abs(data))
print(torch.abs(tensor))

# sin 三角函数
print(np.sin(data))
print(torch.sin(tensor))

# mean 均值
print(np.mean(data))
print(torch.mean(tensor))

data = [[1, 2], [3, 4]]
tensor = torch.FloatTensor(data)
print(np.matmul(data, data))
print(torch.mm(tensor, tensor))

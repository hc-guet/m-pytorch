import torch
import torch.utils.data as Data

torch.manual_seed(1)

BATCH_SIZE = 8

x = torch.linspace(1, 10, 10)
y = torch.linspace(10, 1, 10)

# TensorDataset 可以用来对 tensor 进行打包，就好像 python 中的 zip 功能
# 该类通过每一个 tensor 的第一个维度进行索引
torch_dataset = Data.TensorDataset(x, y)

# for x, y in torch_dataset:
#     print(x, y)

loader = Data.DataLoader(
    dataset=torch_dataset,
    batch_size=BATCH_SIZE,
    shuffle=True,
    # num_workers=1,
)

for epoch in range(3):
    for batch_id, (batch_x, batch_y) in enumerate(loader):
        print('Epoch: ', epoch, '| Batch_id: ', batch_id, '| batch x: ',
              batch_x.numpy(), '| batch y: ', batch_y.numpy())

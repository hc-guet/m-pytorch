import torch

x = torch.Tensor([1, 2, 3]).reshape(3, 1)

net = torch.nn.Sequential(
    torch.nn.Linear(1, 10),
    torch.nn.ReLU(),
    torch.nn.Linear(10, 1)
)

print(net)

out = net(x)
print(out)

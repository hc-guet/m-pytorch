import torch
from torch import nn
import torchvision.datasets as dsets
import torch.utils.data as Data
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

torch.manual_seed(1)

EPOCH = 1
BATCH_SIZE = 256
INPUT_SIZE = 28  # 将宽度作为输入维度大小
TIME_STEP = 28  # 将一张图片的高度作为序列长度
LR = 0.01
DOWNLOAD = True
device = 'cuda'

train_data = dsets.MNIST(
    root='./mnist',
    train=True,
    transform=transforms.ToTensor(),
    download=DOWNLOAD
)

# # 显示数字
# plt.imshow(train_data.data[7], cmap=plt.get_cmap('gray'))
# plt.show()

test_data = dsets.MNIST(root='./mnist', train=False)

train_loader = Data.DataLoader(
    dataset=train_data,
    batch_size=BATCH_SIZE,
    shuffle=True,
)

test_x = torch.unsqueeze(test_data.data, dim=1).type(torch.FloatTensor)[:2000] / 255.
test_y = test_data.targets[:2000]


class RNN(nn.Module):
    def __init__(self):
        super(RNN, self).__init__()
        self.rnn = nn.LSTM(
            input_size=INPUT_SIZE,
            hidden_size=64,
            num_layers=1,
            batch_first=True
        )
        self.out = nn.Linear(64, 10)

    def forward(self, x):
        r_out, (h_n, h_c) = self.rnn(x, None)
        out = self.out(r_out[:, -1, :])
        return out


rnn = RNN().to(device)
print(rnn)

optimizer = torch.optim.Adam(rnn.parameters(), lr=LR)
loss_func = nn.CrossEntropyLoss()

for epoch in range(EPOCH):
    for b_id, (x, b_y) in enumerate(train_loader):
        b_x = x.view(-1, 28, 28)
        b_x, b_y = b_x.to(device), b_y.to(device)
        output = rnn(b_x)
        loss = loss_func(output, b_y)
        print(epoch, b_id, loss.item())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

test_x = test_x.to(device)
test_output = rnn(test_x[:10].view(-1, 28, 28))
pred_y = torch.max(test_output, 1)[1].cpu().data.numpy().squeeze()
print("prediction number", pred_y)
print("real number", test_y[:10].numpy())
